Seguimos los pasos del tuto. 
(Necesitaremos express, mongoose, nodemon y dotenv)

## 1
- Primero creamos un server básiquísimo. Con una ruta de prueba ('/hello').

```
const express = require('express')
const app = express()

//routes 
app.get('/hello',(request,response)=>{
    response.send('Leisure Ledger APP')
})

const port = 3300

app.listen(port,()=>{
    console.log(`Esa peña ahí en el puerto ${port}`)
})
```

## 2

- El segundo paso es diseñar cuáles van a ser las rutas básicas. 

1) En nuestro caso, vamos a querer tener una que nos traiga todos los ledgers que coincidan con un usuario.

2) Una ruta que nos permita crear un ledger. 

3) Otra que nos permita acceder a un ledger en particular. 

4) Otra que nos permita actualizar un ledger. 

5) Y otra que nos permita borrarlo. 

Lo dejamos comentado : (más adelante se decidirá a quitar el '/user' de la ruta)

```
// app.get('/api/v1/user/ledgers')       - get all the ledgers from a User
// app.post('/api/v1/user/ledgers')       - create a new ledger for a user
// app.get('/api/v1/user/ledgers')        - get single ledger
// app.patch('/api/v1/user/ledgers')      - update task
// app.delete('/api/v1/user/ledgers')     - delete task

```

## 3

- El siguiente paso es importante, creamos el routes y el controller que van a ser dos carpetas con archivos. 


**Routes**: 
Se empieza así, luego refactorizamos para usar el controller. 

```
const express = require('express')
const router = express.Router()


router.route('/').get((request,response)=>{
    response.send('all ledgers')
}) 

module.exports= router
```

En la app se ha dicho que tasks va a usar /api/v1/user/ledgers, en este caso, como la ruta en el router es sólo '/'
si no ponemos nada más (es decir sólo /api/v1/user/ledgers) querrá decir que es esta ruta. 


**Controllers**:

Pese a que habíamos diseñado cinco rutas, de momento vamos a hacer dos sólo en los controllers para ver cómo funciona. 

```
const getAllLedgers= (request,response)=>{
    response.send('get all ledgers')
}

const createLedger = (request,response)=>{
    response.json(request.body)
}

module.exports = {
    getAllLedgers,
    createLedger
}
```

En create la respuesta es .json() para poder testear con postman. De momento Controllers y routers no están conectados. Es el paso siguiente. 
Más adelante también lo intentaremos convertir en clase en vez de varias constantes. 

**app.js** queda así: 

```
const express = require('express')
const app = express()
const ledgers = require('./routes/ledgers')

//middleware (sin esto no tendremos la data en request.body)
app.use(express.json())  

//routes 
app.get('/hello',(request,response)=>{
    response.send('Leisure Ledger APP')
})

app.use('/api/v1/user/ledgers', ledgers)
//se gestiona todo desde router y controller (más limpio)
//Le decimos, si se tira esta ruta (/api/v1 etc.) haces lo que haya en ledgers (que es el router que a su vez tira del controller.)

const port = 3300

app.listen(port,()=>{
    console.log(`Esa peña ahí en el puerto ${port}`)
})
```

## 4 
- Vamos a conectar Routes y Controllers. 

En **routes** 

```
const express = require('express')
const router = express.Router()
const {getAllLedgers} = require('../controllers/ledgers')


router.route('/').get(getAllLedgers) 

module.exports= router

```

Como vemos, importamos el objeto de los controllers y se lo pasamos al router. Básicamente le estamos diciendo, si en el endpoint no se añade nada, es decir el endpoint es /api/v1/user/ledgers, devuelve lo que sea que getAllLedgers devuelve (en este caso un string en que pone 'get all ledgers')
Importante que el nombre que importamos de los controllers sea el mismo que allí (me daba error porque importaba getAllTasks, que era el nombre de la const en el tutorial que hice)

Ya de paso, en controllers vamos a terminar los otros métodos. 

Queda así: 

```
const getAllLedgers= (request,response)=>{
    response.send('get all ledgers')
}

const createLedger = (request,response)=>{
    response.json(request.body)
}

const getLedger = (request, response)=>{
    response.json({id:request.params.id})
}

const updateTask = (request, response)=>{
    response.send('update task')
}
const deleteTask = (request, response)=>{
    response.send('delete task')
}

module.exports = {
    getAllLedgers,
    createLedger
}

```

Nos queda la duda de cómo solucionar la parte del /user.

## 5
- Creamos nueva colección en Postman.

Definimos la variable global url en minúscula (en mayus la tenemos en el otro proyecto).
Testeamos el getAllLedgers.

## 6

- Paso breve, en routes, importamos el resto de controllers, de momento como funciones. Luego intentaremos pasarlo a clase. 


Ahora lo suyo es encadenar según rutas, es decir, los que no necesiten id ni nada, van por un sitio, y los que necesiten id, por otro. 
Sin embargo, nosotros tenemos el enigma del user. 
Vamos a hacerlo como si sólo hubiese un usuario. 

Routes queda así: 

```
const express = require('express')
const router = express.Router()
const {getAllLedgers, createLedger, updateLedger, getLedger, deleteLedger} = require('../controllers/ledgers')


router.route('/').get(getAllLedgers).post(createLedger) //es decir, como en app.js decimos que use /api/v1/user/ledgers, si no ponemos nada más, usará uno de estos dos, a petición nuestra
router.route('/:id').get(getLedger).patch(updateLedger).delete(deleteLedger)


module.exports= router
```
En principio routes ya no lo tocaríamos más. Los controladores sí porque están hardcodeados. 

## 7

- Conectar con mongoose

Creamos una carpetita nueva en root de api que se llame db (database, vamos) y dentro de ella, un pequeño archivo que se llame connect.js

Requerimos mongoose y usaremos la connection string que tenemos de proyectos anteriores (para saber cómo acceder, mirar el paso 04 (atlas) de las notas del tuto ).

Recordar sólamente cambiar <password> por nuestro password.  

Y lo de "myfirstDataBase" se puede cambiar también por el nombre que le queramos dar. ¡El interrogante no se quita!

Tras esto creamos el método connect de mongoose va a tener dos argumentos, la *connection string* y las *opciones* que básicamente nos sirven para evitar warnings.

```
const mongoose = require('mongoose')

const connectionString = 'mongodb+srv://Mauri:fakePassword@nodeexpresstuto.lt0f3.mongodb.net/leisure-ledger?retryWrites=true&w=majority'

mongoose
.connect(connectionString, {
    useNewUrlParser:true,
    useCreateIndex:true,
    useFindAndModify:false,
    useUnifiedTopology:true,
})
.then(()=>console.log('Successful connection...'))
.catch((error)=>console.log(error))
```

Obviamente la contraseña que hemos dado es falsa. Y bueno, ahora mismo no estamos conectados a mongoose, porque no ejecutamos el connect en ningún lado, eso será en siguientes pasos.

Spoiler: El then y el catch los acabaremos quitando de aquí porque pronto aparecerán problemas de sincronización.  


## 8

- Connect.js en la app

Se puede conectar si exportamos en connect.js (que si miramos el punto anterior no lo habíamos hecho) y lo ejecutamos poniendo esto en la línea 1 de app.js: 

```
require('./db/connect')
``` 
Sin embargo, nos crea un pequeño problema, y es que no va sincronizado como habíamos adelantado en los spoiler. Se conecta a mongoose y luego al servidor. Pero para qué queremos conectarnos a mongoose si el servidor no va? 

Vamos a optar por otro método: 

Empezamos exportando connect e importándolo en app.js 
Para ello, deberemos transformarlo en una constante que devuelva (importante el return) una promesa.

Queda así: 

```
const mongoose = require('mongoose')

const connectionString = 'mongodb+srv://Mauri:fakePassword@nodeexpresstuto.lt0f3.mongodb.net/leisure-ledger?retryWrites=true&w=majority'


const connectDB = (url)=>{
    return mongoose
    .connect(connectionString, {
        useNewUrlParser:true,
        useCreateIndex:true,
        useFindAndModify:false,
        useUnifiedTopology:true,
    })
}

module.exports = connectDB
```

Hemos quitado como decíamos el then y el catch. 

Volviendo a la app, importaremos el connect, ahora bien, sin ejecutarlo para que no pase lo que hemos dicho. 

y crearemos el start y lo declararemos para que al runear npm start, se ejecute. 

App.js quedaría así: 

```
const express = require('express')
const app = express()
const ledgers = require('./routes/ledgers')
const connectDB = require('./database/connect')

//middleware
app.use(express.json())

//routes 
app.get('/hello', (request, response) => {
    response.send('Leisure Ledger APP')
})

app.use('/api/v1/user/ledgers', ledgers)




const port = 3300

const start = async () => {
    try {
        await connectDB()

        app.listen(port, () => {
            console.log(`Esa peña ahí en el puerto ${port}`)
        })
    } catch (error) {
        console.log(error)
    }
}

start()

```
En este momento, crashea porque la contraseña sigue siendo falsa. Si la cambiamos momentáneamente por la verdadera, funciona. 

## 9

- .env

Con esto solucionaremos el problema de la contraseña, puesto que irá al gitignore. 

Creamos un archivo .env en raíz de root. 
El contenido será tal que así: 

```
MONGO_URI = mongodb+srv://Mauri:fakePassword@nodeexpresstuto.lt0f3.mongodb.net/leisure-ledger?retryWrites=true&w=majority

```
La contraseña sigue siendo falsa, aquí, después de pegar esto, se cambia por la verdadera. 
En connect eliminamos la constante connectionString, puesto que ahora usaremos la .env  en mongoose.connect, cambiamos el parámetro connectionString que ya no existe, por url, que se la pasaremos desde app.js.

Y la conexión vía .env la haremos en app.js. 

Habrá que requerir dotenv y su método config()
y quedaría tal que así: 

```

const express = require('express')
const app = express()
const ledgers = require('./routes/ledgers')
const connectDB = require('./database/connect')
require('dotenv').config()

//middleware
app.use(express.json())

//routes 
app.get('/hello', (request, response) => {
    response.send('Leisure Ledger APP')
})

app.use('/api/v1/user/ledgers', ledgers)




const port = 3300

const start = async () => {
    try {
        await connectDB(process.env.MONGO_URI)

        app.listen(port, () => {
            console.log(`Esa peña ahí en el puerto ${port}`)
        })
    } catch (error) {
        console.log(error)
    }
}

start()

```













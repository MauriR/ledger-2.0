const express = require('express')
const router = express.Router()
const Ledger = require('../controllers/ledgers')
const ledger = new Ledger

router.route('/').post(ledger.createLedger) //es decir, como en app.js decimos que use /api/v1/user/ledgers, si no ponemos nada más, usará uno de estos dos, a petición nuestra
router.route('/ledger/:user/:name').get(ledger.getLedger).patch(ledger.updateLedger).delete(ledger.deleteLedger)
router.route('/user/:user').get(ledger.getAllLedgers)

module.exports= router
const express = require('express')
const router = express.Router()
const User = require('../controllers/users')
const user = new User

router.route('/').post(user.createUser)
router.route('/:id').get(user.getUser).delete(user.deleteUser)

module.exports= router
const USER = require('../models/Users')


class User {

    async getUser(request, response) {
        try {
            const { id: userID } = request.params
            const user = await USER.findOne({ _id: userID })
            //Sabré yo la id del usuario? O busco por nombre? Me cuadra más lo del nombre, pero se puede probar por id y si no, se cambia. 

            if (!user) {
                return response.status(404).json({ message: `We cannot find the user with id ${userID}` })
            }
            response.status(200).json({ user })

        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

    async deleteUser(request, response) {
        try {
            const { id: userID } = request.params
            const user = await USER.findOneAndDelete({ _id: userID })
            if (!user) {
                return response.status(404).json({ message: `We cannot find the user with id ${userID}` })
            }
            response.status(200).json({user})
        } catch (error) {
            response.status(500).json({ message: error })

        }
    }

    async createUser(request, response) {
        try {
            const {user:userName} = request.params
            const existingUser = await USER.findOne({ name: userName })
            const user = await USER.create(request.body)
            if(existingUser){
                return response.status(409).json({message: `The user ${existingUser.user} already exists`})
            }
            response.status(201).json({ user })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }
}

module.exports = User
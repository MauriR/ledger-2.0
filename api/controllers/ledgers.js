const LEDGER = require('../models/Ledgers')


class Ledger {
    async getAllLedgers(request, response) {
        try {
            const { user: ledgerUser } = request.params
            const ledgers = await LEDGER.find({ user: ledgerUser }).exec()
            response.status(200).json({ ledgers })

        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

    async createLedger(request, response) {

        try {
            const ledger = await LEDGER.create(request.body)
            response.status(201).json({ ledger })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }


    async getLedger(request, response) {
        try {
            const ledgerName = request.params.name
            const user = request.params.user
            const ledger = await LEDGER.findOne({ name: ledgerName, user: user })
            if (!ledger) {
                return response.status(404).json({ message: `No ledger with name: ${ledgerName}` })
            }
            response.status(200).json({ ledger })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

    async deleteLedger(request, response) {
        console.log('Se pide delete', request)
        try {
            const user = request.params.user
            const ledgerName= request.params.name
            const ledger = await LEDGER.findOneAndDelete({ name: ledgerName, user:user })
            console.log(ledger)
            if (!ledger) {
                return response.status(404).json({ message: `No ledger with name: ${ledgerName}` })
            }
            response.status(200).json({ledger:null, status:'success'})
        } catch (error) {
           
            response.status(500).json({ message: error })
        }
    }

    async updateLedger(request, response) {
        console.log('apdeit', request)
        try {
            const user = request.params.user
            const ledgerName= request.params.name
            const ledger = await LEDGER.findOneAndUpdate({ name: ledgerName, user:user }, request.body, {
                runValidators: true,
            })
            if (!ledger) {
                return response.status(405).json({ message: `No ledger with id: ${ledgerID}` })
            }
            response.status(200).json({ ledger })

        } catch (error) {
            response.status(500).json({ message: error })
        }
    }
}

module.exports = Ledger
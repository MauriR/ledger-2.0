## 29

- Seguimos ahora con el delete. 

El borrado de ledgers se hace desde la view 'temporal'. El método de axios que le pasaremos en este caso será el delete. 

Nos atascamos en un fallo porque los dos parámetros los habíamos igualado a user. Pierdo un montón de tiempo con la tontería. 

El método en **frontend** queda así: 

```
const deleteLedger = async () => {
      const user = state.details.user
      const ledgerName = store.state.ledger.ledger
      await axios.delete(
        `http://localhost:3300/api/v1/ledgers/ledger/${user}/${ledgerName}`
      )
      store.dispatch('setLedger', null)
      router.replace('/storage')
    }
```

El controller queda así: 

```
  async deleteLedger(request, response) {
        try {
            const user = request.params.user
            const ledgerName= request.params.name
            const ledger = await LEDGER.findOneAndDelete({ name: ledgerName, user:user })
            if (!ledger) {
                return response.status(404).json({ message: `No ledger with name: ${ledgerName}` })
            }
            response.status(200).json({ledger:null, status:'success'})
        } catch (error) {
           
            response.status(500).json({ message: error })
        }
    }

```
El fallo era haber puesto "const ledgerName = request.params.user"



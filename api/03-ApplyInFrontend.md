
## 24

No sé si me mola cómo lo hace el tipo, yo preferiría enganchar en plan trivial, crear las funciones y llamar a las de la api desde el front. 
Empieza creando middleware para poder usar los archivos estáticos. 

app.use(express.static('../public'))

**A partir de aquí tomo el riesgo de hacerlo 'a mi manera' sin seguir los pasos del tuto al 100% .** 
Aunque he visto cosas útiles como quitar el doble try catch etc. 


## 25

- Un problema que aparece luego es el de las solicitudes cruzadas (CORS). Por defecto, no se permiten peticiones de un dominio al otro y puesto que tenemos la app en un puerto (8080) y la api en otro (3300), si hacemos peticiones a la api desde la app, nos la bloquean. 
Para solucionar esto existe CORS. Se puede instalar (npm install cors).
const cors = require('cors') en los imports

app.use(cors()) en el middleware, ambos en app.js de la api. 

## 26
- Tras mucho ensayo y horror, consigo que me cree ledgers. 

Lo hacemos añadiendo un bloque try/catch al createLedger

```
    try {
        axios.post('http://localhost:3300/api/v1/ledgers',  ledger) 
     } catch (error) {
        console.log(error)
    }

```


Hacemos un primer pequeño refactor : 

Creando un método propio para guardar en database: 

```
 const saveInDB = (route, data) => {
      try {
        axios.post(route,  data) 
      } catch (error) {
        console.log(error)
      }
    }
```

Nos llevamos el método a un servicio nuevo que llamamos RequesterService. 
Creamos una clase, donde querremos meter todas las peticiones y la exportamos. 

```
import axios from 'axios'

class Requester {
    createInDB(route, data) {
        try {
            axios.post(route, data)
        } catch (error) {
            console.log(error)
        }
    }
}

export default Requester

```

En createLedger sólo tenemos que importar, declarar y llamar. 

```
import Requester from '../services/RequesterService' //En los imports

const  requester = new Requester()
const postRequest = 'http://localhost:3300/api/v1/ledgers' //En el Setup

requester.createInDB(postRequest, ledger)//Dentro de la función createLedger

```


Queda más limpio, aseado y accesible. Además favorece el cambio de base de datos. 


## 27 
- Con respecto al getAllLedgers, nos toca explotarnos la cabeza para encontrar un modo de pasarle tanto el usuario, como para luego mostrar en la app los botoncitos con nombres de las listas. 

Tras varios intentos, lo conseguimos haciendo lo siguiente: 

```
onBeforeMount(async()=>{
     try{
      const {data: {ledgers}} = await axios.get(`http://localhost:3300/api/v1/ledgers/user/${user}`) //Esto no lo entiendo del todo
       if (ledgers.length > 0) {
        for(let list of ledgers){
          state.list.push(list.name)
        }
       }
     
    }catch(error){
        console.log(error)

    }
     
  })


```

Los valores tanto de user como de sate.list ya estaban creados y guardados en vuex. Por lo tanto, lo que decimos es, en la ruta, me unes el nombre del usuario y es lo que buscará en el backend (hay que tener en cuenta, que hemos realizado cambios en controllers y router )

*Controllers* queda así: 


```

 async getAllLedgers(request, response) {
        console.log(request.params)
        try {
            const  { user: ledgerUser }  = request.params 
            const ledgers = await LEDGER.find({ user: ledgerUser }).exec()
            console.log(ledgers)
            response.status(200).json({ ledgers })
            
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

```

    
    Vemos el exec y cómo indicamos que el usuario viene desde los params. 

Mientras que el *route* queda así: 

    ```
router.route('/user/:user').get(ledger.getAllLedgers)
```

Llega el **refactor** :

Es evidente que si nos llevamos toda la función del hook a  RequesterService, no sabrá lo que es state.list por lo que sólo nos llevamos una parte del hook, la de hacer la petición y queda así: 


```
 getUserLedgers(route) {
        try {
          return  axios.get(route)
            
        } catch (error) {
            console.log(error)

        }
    }

```

    
    Quitamos el async/await porque se queda en la declaración en el front. 

    Por su parte, la función en el hook queda del siguiente modo: 

    ```
    onBeforeMount(async () => {
      const {
        data: { ledgers },
      } = await requester.getUserLedgers(getRequest)

      if (ledgers.length > emptyList) {
        for (let list of ledgers) {
          state.list.push(list.name)
        }
      }
    })
    ```
    Por supuesto, emptiList está declarado en el setup y es igual a 0. 

## 28
- Get single ledger

Cuesta también, por las intromisiones entre rutas sobretodo, pero al final queda así: 

**El método en storage**

En el render del htlm, el @click le pasamos en ledger (como está definido por el v-for, se saca el índice y tal de ahí)

```
const updateInfo = async(ledger)=>{
      const index = state.list.indexOf(ledger)
      const ledgerName=  state.list[index]
      await axios.get(`http://localhost:3300/api/v1/ledgers/ledger/${user}/${ledgerName}`)
    
    }
```

**La nueva ruta**

Se le añade un ledger extra

```
router.route('/ledger/:user/:name').get(ledger.getLedger).patch(ledger.updateLedger).delete(ledger.deleteLedger)
```


**El nuevo controller**

```
async getLedger(request, response) {
        try {
            const ledgerName = request.params.name
            const user = request.params.user
            const ledger = await LEDGER.findOne({ name: ledgerName, user: user })
            if (!ledger) {
                return response.status(404).json({ message: `No ledger with name: ${ledgerName}` })
            }
            response.status(200).json({ ledger })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }
```

- Refactor

El código del **requester** era idéntico al que ya había en el getAllLedgers, por lo que los fusionamos en un mismo método: 

```
obtainRequestedInfo(route) { 
        try {
            return axios.get(route)

        } catch (error) {
            console.log(error)

        }
    }
```

En **storage** no es fácil hacer un refactor, pero algo se puede hacer:

```
const updateInfo = async (ledger) => {
      const ledgerName = findLedger(ledger)
      const requestedLedger = await requester.obtainRequestedInfo(
        getSingleLedgerRequest + `/${ledgerName}`
      )
      const selectedLedger = {
        user: requestedLedger.data.ledger.user,
        listName: requestedLedger.data.ledger.name,
        partakers: requestedLedger.data.ledger.partakers,
        expenses: requestedLedger.data.ledger.expenses,
      }
      store.dispatch('setLedger', selectedLedger)
      router.replace('/temporal')
```

    
    Hemos sacado la ruta, excepto el nombre del ledger. (const getSingleLedgerRequest = `http://localhost:3300/api/v1/ledgers/ledger/${user}` )
    
     La búsqueda del nombre del ledger la realizamos mediante una función externa (findLedger) a la que le pasamos el ledger que le viene por el evento. 
```
const findLedger = (ledger) => {
      const index = state.list.indexOf(ledger)
      return state.list[index]
    }
```



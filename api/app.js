
const express = require('express')
const app = express()
const cors = require('cors')
const ledgers = require('./routes/ledgers')
const user = require('./routes/users')
const connectDB = require('./database/connect')
require('dotenv').config()

//middleware
app.use(express.json())
app.use(cors())
app.use(express.static('../public'))

//routes 
app.use('/api/v1/ledgers', ledgers)
app.use('/api/v1/user', user)

const port = 3300

const start = async () => {
    try {
        await connectDB(process.env.MONGO_URI)

        app.listen(port, () => {
            console.log(`Esa peña ahí en el puerto ${port}`)
        })
    } catch (error) {
        console.log(error)
    }
}

start()

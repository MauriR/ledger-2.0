const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  user:{
      type:String,
      required:[true, 'A valid user name must be provided'],
      trim: true,
  },  
})

module.exports = mongoose.model('User', UserSchema)
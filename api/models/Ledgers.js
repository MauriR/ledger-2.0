const mongoose = require('mongoose')

const LedgerSchema = new mongoose.Schema({
    user:{
        type:String,
        required:[true, 'A valid name must be provided']
    },
    name:{
        type:String,
        required:[true, 'You must provide a valid ledger name' ],
        trim:true,
        maxlength:[25, "The ledger's name cannot be more than 25 characters"]
    },
    partakers:{
        type:Array
    },
    expenses:{
        type:Array
    },


})

module.exports = mongoose.model('Ledger', LedgerSchema)


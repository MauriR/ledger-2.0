Con los pasos anteriores, la app queda de momento así. 
Ahora vamos a ver lo que serían schemas y models. 
Antes de seguir, como paso 9.1, podemos terminar de crear las rutas en postman. 


## 10 

- Models y schemas

Creamos carpetita nueva de nuevo en el root de api. Esta vez con el nombre models. 

Dentro de models, un archivo llamado Ledgers.js

Qué tratamos de hacer en este paso? Que todos los ledgers tengan la misma *estructura* (nombre, gastos, participantes etc.)

En este 'primer' paso lo hacemos facilito. 

```
const mongoose = require('mongoose')

const LedgerSchema = new mongoose.Schema({
  user:String,  name:String, partakers:Array, expenses:Array
})

module.exports = mongoose.model('Ledger', LedgerSchema)

```

Todo lo que enviemos a la base de datos deberá tener un usuario, un nombre, unos participantes y unos gasto. Si se nos ha olvidado algo, se puede poner luego. 
Si enviamos otro tipo de data, no lo subirá a la base de datos. 

Hay que tener en cuenta, que en este momento, el schema está exportado pero no importado. No se está usando. 
Más adelante se refactorizará y quedará más limpito y fácil. 


## 11

- Model en nuestro controller 

Importamos el modelo a nuestro controlador. 
Veremos un ejemplo de cómo se usa con el createLedger. 

En vez de copiar todo el archivo controllers, copio sólo el createLedger, puesto que lo otro que ha cambiado es el import y es un import normal (aunque Ledger lo declaramos en mayúsculas)

```
const createLedger = async (request, response)=>{
    const ledger = await Ledger.create(request.body)
    response.status(201).json({ledger})
}

```

Así queda. Lo testeamos en postman y funciona: Si le pasamos propiedades que no están declaradas en el schema, no las sube. 


## 12 

- Validación en los schema. 

La validación es un océano y vamos a ver una gota, pero básicamente vamos a evitarnos problemas como que se pueda enviar campos vacíos etc. 

Lo que vamos a hacer es convertir el esquema en un objeto. 

```
const mongoose = require('mongoose')

const LedgerSchema = new mongoose.Schema({
    user:{
        type:String,
        required:[true, 'A valid name must be provided']
    },
    name:{
        type:String,
        required:[true, 'You must provide a valid ledger name' ],
        trim:true
        maxlength:[25, 'The ledger's name cannot be more than 25 characters']
    },
    partakers:{
        type:Array
    },
    expenses:{
        type:Array
    },


})

module.exports = mongoose.model('Ledger', LedgerSchema)
```
Tomamos name: Ahora le pedimos que el tipo sea un string, de lo contrario no lo aceptará. Le decimos que es imprescindible que el ledger tenga un nombre, de lo contrario saltará el error que hay a continuación. 
Trim lo que hace es quitar espacios en blanco innecesarios. 

Existen muchas otras cosas, como por ejemplo la opción 'default' que será el valor por defecto (en una lista de tareas, la opción completed se puede setear a false por defecto puesto que al crearla no estará hecha).

Testeamos el trim y todo (con postman) y funciona.

Sin embargo, si ahora enviamos un nombre vacío, nos da error porque lo igual a false y en required lo tenemos en true. Se queda pensando. 

Veremos en el siguiente paso cómo solucionarlo. 


## 13
- Try/catch block en los controllers

El error del que hablábamos en el punto anterior nos da *unhandled promise rejection* y se origina en  los controladores, por el await que no tiene el try/catch block. Vamos a solucionarlo. 

```
const createLedger = async (request, response)=>{
   try{
    const ledger = await Ledger.create(request.body)
    response.status(201).json({ledger})
   }catch(error){
       response.status(500).json({message:error})
   }
}
```

Con esto, si enviamos en blanco ya no se queda pensando, devuelve el error. Pero ahora se nos plantean otras cosas. 

Dudas rápidas:

1) Debemos wrapear toda la lógica de todos nuestro controllers en try/catch? - De momento sí. Más adelante ya lo simplificaremos

3) El error que devolvemos es larguísimo, se puede simplificar? - Sí, pero dejamos el gigante de momento porque nos sirve para mostrar los errores exactos que estamos teniendo. 

De momento con los models hemos terminado también.


## 14

- controlador de getAllLedgers

Ahora lo que queremos es leer (la R de CRUD).
Dentro de todas las opciones query (que vemos en la docu de mongoose) parece que find() va a ser la nuestra. Find puede aceptar parámetros de búsqueda. Si lo enviamos vacío, envía todo lo que hay en la base de datos. 

Queda así: 

```
const getAllLedgers= async (request,response)=>{
    try{
        const ledgers = await Ledger.find({user:'Rubén'})//algo que nos busque por user
        response.status(200).json({ledgers})
    }catch{
        response.status(500).json({message:error})
    }
}
```

Funciona porque en las rutas, decíamos que get sin añadir nada, era igual a esta función (getAllLedgers). Por eso no precisa nada. 
El usuario, eso sí, está harcodeado de momento. A falta de investigación. 

Seguimos peleando y probando así sí funciona:

```
const {user: ledgerUser} = request.body
const ledgers = await Ledger.find({user: ledgerUser})

```


## 15

- Controlador de getLedger

En nuestra app, esta función proporciona info sobre Ledgers específicos. 
Puesto que busca por ID, vamos a tener que implementar una response específica si la id que le pasamos no cuadra con niguna de las tasks  que actualmente tenemos (luego podremos refactorizar también quizá el getAllLedgers si no cuadra el usuario)
En este caso, el query function que nos interesa parece que va a ser .findOne()
.findOne acepta parámetros, o conditions que serán objetos. 


```

const getLedger = async (request, response) => {
    try {
        const { id: ledgerID } = request.params
        const ledger = await Task.findOne({ _id: ledgerID })
        if (!task) {
            return response.status(404).json({ message: `No ledger with id: ${ledgerID}` })
        }
        response.status(200).json({ ledger })
    } catch (error) {
        response.status(500).json({ message: error })
    }
}

```

Para testarlo, en la ruta, debemos añadir la id de alguno de los ledger que tenemos. 

Hay dos errores porque uno es si se pide algo que sintácticamente podría existir pero no coincide (error del que busca) y el 500 genérico es un CastError, no tiene sintaxis correcta (p.ej.: el número de carácteres no encaja)
Testeado, funciona. 

## 16

- Controlador de deleteLedger

Seguimos por este y dejamos el de update para el final porque será un pelín más complejo. 

Delete será parecido al find, pero el query de mongoose nos ofrece findOneAndDelete, el cual, como su nombre indica, nos facilitará las cosas bastante. 

```
const deleteLedger = async (request, response) => {
    try{
        const {id: ledgerID}=request.params
        const ledger = await Ledger.findOneAndDelete({_id:ledgerID})
if(!ledger){
    return response.status(404).json({message:`No ledger with id: ${ledgerID}`})
}
response.status(200).json({ledger})
    }catch(error){
        response.status(500).json({message:error})
    }
}

```
Hemos dejado esa response, porque es nuestra primera API y mola verlo. Pero no estamos limitado a ello. 
Otras opciones hubiesen sido: 
a) response.status(200).send()
b) response.status(200).json({task:null, status:'success'})

Sin embargo, como hemos comentado, nos quedamos el actual porque en postman podemos ver qué hemos quitado. 

Testeado y funciona! (Cuidado que borra de verdad xD)

## 17 

- Controller de updateTask

Va a tener más funcionalidad porque vamos a necesitar también el body (ya que vamos a updatear). 
También debemos pasarle opciones, ya que por defecto no tendremos los validators trabajando y no obtendremos el ítem que hemos updateado. 
En el tuto veíamos un paso intermedio antes del final, aquí vamos a ir directamente al bueno. 


```
const updateLedger = async (request, response) => {
    try {
        const { id: ledgerID } = request.params
        const ledger = await Ledger.findOneAndUpdate({ _id: ledgerID }, request.body, {
            new: true,
            runValidators: true,
        })
        if (!ledger) {
            return response.status(404).json({ message: `No task with id: ${ledgerID}` })
        }
        response.status(200).json({ ledger })

    } catch (error) {
        response.status(500).json({ message: error })
    }
}

```

En este paso, en el await, a parte de la id para que busque, le metemos la información nueva. 
findOneAndUpdate acepta un tercer parámetro que serían las opciones,las ponemos porque sino ¡nos devuelve el valor original! (con su status 200) sin embargo si clickásemmos en getAllTasks, SÍ habría cambiado. 

Con respecto a éstas queremos dos cosas: Recibir la nueva info, y que se apliquen los validatores (si enviamos un nombre vacío, nos da successful)


Testeado y funciona.



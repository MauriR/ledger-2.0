Intentamos hacer lo mismo con User. Esta vez, decidimos hacerlo como clase. 

## 18 

Empezamos por los pasos básicos. 

**user controllers**

```
class User {
    createUser (request,response){
        response.json(request.body)
    }

    getUser(request,response){
        response.json({id:request.params.id})
    }

     deleteUser(request,response){
       response.send('User Deleted Successfully')
    }
}

module.exports = User

```

**user routes**

```
const express = require('express')
const router = express.Router()
const User = require('../controllers/users')
const user = new User()

router.route('/test').get(user.deleteUser) //Entra en conflicto. 

module.exports= router
```

Como vemos sólo está activada la funcion deleteUser. Es la única que testeamos en este punto. 

**app**

Añadimos estas dos líneas:

```
const user = require('./routes/users')
```
En los imports 


```
app.use('/api/v1/user/user', user)
```
En el middleware

Al principio fallaba porque no había puesto el middleware. 
Testeado (manualmente, escribiendo la ruta, sin postman). Funciona. 

## 19

- Una vez visto que ha funcionado en users, refactorizamos controllers de ledger. 

Lo pasamos también a clase. 


**ledgers controllers**

El archivo que más varía.  Queda así: 

```
const LEDGER = require('../models/Ledgers')


class Ledger {
    async getAllLedgers(request, response) {
        try {
            const { user: ledgerUser } = request.body
            const ledgers = await LEDGER.find({ user: ledgerUser })
            response.status(200).json({ ledgers })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

    async createLedger(request, response) {
        try {
            const ledger = await LEDGER.create(request.body)
            response.status(201).json({ ledger })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }


    async getLedger(request, response) {
        try {
            const { id: ledgerID } = request.params
            const ledger = await LEDGER.findOne({ _id: ledgerID })
            if (!ledger) {
                return response.status(404).json({ message: `No ledger with id: ${ledgerID}` })
            }
            response.status(200).json({ ledger })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

    async deleteLedger(request, response) {
        try {
            const { id: ledgerID } = request.params
            const ledger = await LEDGER.findOneAndDelete({ _id: ledgerID })
            if (!ledger) {
                return response.status(404).json({ message: `No ledger with id: ${ledgerID}` })
            }
            response.status(200).json({ ledger })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

    async updateLedger(request, response) {
        try {
            const { id: ledgerID } = request.params
            const ledger = await LEDGER.findOneAndUpdate({ _id: ledgerID }, request.body, {
                new: true,
                runValidators: true,
            })
            if (!ledger) {
                return response.status(404).json({ message: `No task with id: ${ledgerID}` })
            }
            response.status(200).json({ ledger })

        } catch (error) {
            response.status(500).json({ message: error })
        }
    }
}

module.exports = Ledger

```


**Ledger router** 

QUeda parecido al de user:

```
const express = require('express')
const router = express.Router()
const Ledger = require('../controllers/ledgers')
const ledger = new Ledger

router.route('/').get(ledger.getAllLedgers).post(ledger.createLedger) //es decir, como en app.js decimos que use /api/v1/user/ledgers, si no ponemos nada más, usará uno de estos dos, a petición nuestra
router.route('/:id').get(ledger.getLedger).patch(ledger.updateLedger).delete(ledger.deleteLedger)


module.exports= router

```



## 20
- Creamos el model de Users

```
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  user:{
      type:String,
      required:[true, 'A valid user name must be provided']
  },  
})

module.exports = mongoose.model('User', UserSchema)

```

Lo importamos a los controllers de Users y de momento no hacemos más. 


## 21

- Implementamos models en los controllers de user. 

Empezamos por la de createUser, será prácticamente igual que la de Ledger:

```
async createUser(request,response){
        try{
            const user = await USER.create(request.body)
            response.status(201).json({user})
        }catch(error){
            response.status(500).json({message:error})
        }
    }
```
 

    Luego ya las otras dos que también serán casi idénticas: 



    ```
 async deleteUser(request, response) {
        try {
            const { id: userID } = request.params
            const user = await USER.findOneAndDelete({ _id: userID })
            if (!user) {
                return response.status(404).json({ message: `We cannot find the user with id ${userID}` })
            }
            response.status(202).json({ message: error })
        } catch (error) {

        }
    }

   async getUser(request, response) {
        try {
            const { id: userID } = request.params
            const user = await USER.findOne({ _id: userID })
            //Sabré yo la id del usuario? O busco por nombre? 

            if (!user) {
                return response.status(404).json({ message: `It does'n exist a user with id ${userID}` })
            }
            response.status(200).json({ user })

        } catch (error) {
            response.status(500).json({ message: error })
        }
    }

    ```

    De momento no vamos a dar la opción de actualizar o cambiar el nombre de usuario. 

    Testeado con el postman. createUser() Y deleteUser() funcionan, no así getUser(). Intento averiguar por qué. Se había colado un ledger en vez de user. 



 ## 22
 - Pequeñísimo refactor de las rutas en app.js

Nueva apariencia: 

```
app.use('/api/v1/ledgers', ledgers)
app.use('/api/v1/user', user)
```

Hay que cambiar también la variable global de Postman (quitar el /user ).


## 23

- Otro mini-refactor: hacemos que createUser no deje crear usuarios duplicados: 

```
async createUser(request, response) {
        try {
            const {user:userName} = request.params
            const existingUser = await USER.findOne({ name: userName })
            const user = await USER.create(request.body)
            if(existingUser){
                return response.status(409).json({message: `The user ${existingUser.user} already exists`})
            }
            response.status(201).json({ user })
        } catch (error) {
            response.status(500).json({ message: error })
        }
    }
    ```
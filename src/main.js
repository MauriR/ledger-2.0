import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

const firebaseConfig = {
  apiKey: "AIzaSyCy40uFvdB3tm5f_mN4noPaHBrLoMdnAaY",
  authDomain: "leisure-dredger.firebaseapp.com",
  databaseURL: "https://leisure-dredger-default-rtdb.firebaseio.com",
  projectId: "leisure-dredger",
  storageBucket: "leisure-dredger.appspot.com",
  messagingSenderId: "484519909443",
  appId: "1:484519909443:web:c818f1b3190819fc3e9cf6"
}

firebase.initializeApp(firebaseConfig)
createApp(App).use(router).use(store).use(VueSweetalert2).mount('#app')

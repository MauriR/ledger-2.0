import { createStore } from 'vuex'

export default createStore({
    state: {
        user: null,
        ledger: null,
        expenses: [],
        editing: false,
        editingExpense: null,
        index: null
    },

    //funciones que afectan al estado (state)
    mutations: {
        SET_USER(state, user) {
            state.user = user
        },//cuando runeamos esta mutación en action, estamos pasándole un user y vamos a setear el state object al user que estamos pasando. 

        SET_LEDGER(state, ledger) {
            console.log('asked set ledger', ledger)
            state.ledger = ledger
            console.log(state.ledger, 'bifor setting')
        },

        SET_EXPENSES(state, expenses) {
            state.expenses.push(expenses)
        },

        RESET_EXPENSES(state, ) {
            state.expenses = []
        },

        UPDATE_EXPENSES(state, index){
            state.expenses.splice(index,1)
        },

        EDIT(state, index){
            state.editing = true
            state.index= index
        },

        NO_EDITING(state){
            state.editing = false
        },

        EDITING_EXPENSE(state,index){
            state.editingExpense = state.expenses[index]
        },

        EDIT_EXPENSE(state,expense){
            state.expenses.splice(state.index,1,expense)
        },
        FULL_RESET(state){
            console.log('reset2')
        state.user= null
        state.ledger= {name:'', partakers:[]}
        state.expenses= []
        state.editing=  false
        state.editingExpense= null
        state.index= null
        }
    },


    actions: {
        setUser({ commit }, user) {
            commit('SET_USER', user)
        },

        setLedger({ commit }, ledger) {
            commit('SET_LEDGER', ledger)
        },

        setExpenses({ commit }, expenses) {
            commit('SET_EXPENSES', expenses)
        },

        resetExpenses({ commit }) {
            commit('RESET_EXPENSES')
        },

        updateExpenses({commit}, index){
            commit('UPDATE_EXPENSES',index)
        },
        edit({commit}, index){
            commit('EDIT', index)
            commit('EDITING_EXPENSE', index)
           
        },

        noEditing({commit}){
            commit('NO_EDITING')
        },

        editingExpense({commit},index){
            commit('EDITING_EXPENSE', index)
        },
        editExpense({commit}, expense){
            commit('EDIT_EXPENSE', expense)
        },

        fullReset({commit}){
            console.log('reset')
            commit('FULL_RESET')
        }
    },//funciones que se llaman a través de la app que llaman a las mutaciones
    //commit es la palabra con la que referenciamos a una mutación

    modules: {

    }
})

//en los componenentes no se llama directamente a las mutaciones
//La explicación de por qué no almacena directamente en state es muy larga y técnica. Nos recomienda seguir el standard. 
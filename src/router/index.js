import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/login',
    name: 'Login',
    component: ()=> import('../views/Login.vue')
  },

  {
    path: '/register',
    name: 'Register',
    component: ()=> import('../views/Register.vue')
  },

  {
    path: '/create',
    name: 'Create',
    component: ()=> import('../views/CreateLedger.vue')
  },

  {
    path: '/temporal',
    name: 'Temporal',
    component: ()=> import('../views/Temporal.vue')
  },

  {
    path: '/storage',
    name: 'Storage',
    component: ()=> import('../views/Storage.vue')
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


export default router
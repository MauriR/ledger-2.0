import firebase from 'firebase'

export let registerService = function () {
    let register = (email, password) => {
        firebase
            .auth()
            .createUserWithEmailAndPassword(email.value, password.value)
            .then(user => {
                alert(user)
            })
            .catch(err => alert(err.message))
    }
    return { register }
}
export default registerService
import firebase from "firebase"
import { useStore } from 'vuex'


export let logService = function () {
    const store = useStore()
    let login = (email, password) => {
        firebase
            .auth()
            .signInWithEmailAndPassword(email.value, password.value)
            .then((data) => console.log(data))
            .catch((err) => alert(err.message));
    }

    let logout = function () {
        firebase
            .auth()
            .signOut()
            .then(() => console.log('Signed Out'))
            .catch((err) => alert(err.message))
        store.dispatch('setUser', null)
        store.dispatch('setLedger', null)
        store.dispatch('resetExpenses')

    }
    return { login, logout }
}
export default logService
import axios from 'axios'

class Requester {
    createInDB(route, data) {
        try {
            axios.post(route, data)
        } catch (error) {
            console.log(error)
        }
    }


    obtainRequestedInfo(route) { //se fusionan obtener todos los Ledgers y obtener uno sólo ya que el código era duplicado.
        try {
            return axios.get(route)

        } catch (error) {
            console.log(error)

        }
    }

  
}

export default Requester

